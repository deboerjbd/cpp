#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

struct getallenlijst {
        int getal1;
        int getal2;
        int getal3;
        getallenlijst* next;
        };

int main() {
	srand(time(NULL)); //Create random nummer, seed is time voor randomize.


getallenlijst* n;		//maak pointer aan voor n. Maken hiermee nieuwe structs aan.
getallenlijst* t;		//maak pointer aan voor t. Linken hiermee de boel aan elkaar.
getallenlijst* h;		//maak hiermee een start punt.

n = new getallenlijst;		//maak nieuwe struct aan.
n->getal1 = rand();		//vul met data. 
n->getal2 = rand();
n->getal3 = rand();

t = n;		//definieer t voor linken van lists.
h = n;		//definieer h zodat we altijd terug naar start kunnen.

n = new getallenlijst;
n->getal1 = rand();
n->getal2 = rand();
n->getal3 = rand();

t->next = n; // verplaats t naar voren van oude struct.
t = n; //verplaats t weer naar de next in struct.

n = new getallenlijst;
n->getal1 = rand();
n->getal2 = rand();
n->getal3 = rand();

t->next = n; // verplaats t naar voren van oude struct.
t = n; //verplaats t weer naar de next in struct.

n = new getallenlijst;
n->getal1 = rand();
n->getal2 = rand();
n->getal3 = rand();

t->next = n; // verplaats t naar voren van oude struct.

t = n; //verplaats t weer naar de next in struct.
n = new getallenlijst;
n->getal1 = rand();
n->getal2 = rand();
n->getal3 = rand();

t->next = n;		//verplaats t weer.
n->next = NULL;		//Eindig lijst.


//Loop om getallen uit te printen.
getallenlijst *current = h;
while( current != NULL ) {
cout << (*current).getal1 << endl;
cout << (*current).getal2 << endl;
cout << (*current).getal3 << endl;
current = current->next;
}

//Loop om alle nodes te verwijderen om memory leak te voorkomen.
while( current != NULL ) {
delete n;
current = current->next;
}

//	delete n;
		return 0;
}
