#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

struct getallenlijst {
        int getal1;
        int getal2;
        int getal3;
        getallenlijst* next;
        };

int main() {
        srand(time(NULL)); //Create random nummer, seed is time voor randomize.


getallenlijst* nieuw;               //maak pointer aan voor n. Maken hiermee nieuwe structs aan.
getallenlijst* temp;               //maak pointer aan voor t. Linken hiermee de boel aan elkaar.
getallenlijst* head;               //maak hiermee een start punt.

nieuw = new getallenlijst;          //maak nieuwe struct aan.
nieuw->getal1 = rand();             //vul met data.
nieuw->getal2 = rand();
nieuw->getal3 = rand();

temp = nieuw;          //definieer t voor linken van lists.
head = nieuw;          //definieer h zodat we altijd terug naar start kunnen.

nieuw = new getallenlijst;
nieuw->getal1 = rand();
nieuw->getal2 = rand();
nieuw->getal3 = rand();

temp->next = nieuw; // verplaats t naar voren van oude struct.
temp = nieuw; //verplaats t weer naar de next in struct.

nieuw = new getallenlijst;
nieuw->getal1 = rand();
nieuw->getal2 = rand();
nieuw->getal3 = rand();

temp->next = nieuw; // verplaats t naar voren van oude struct.
temp = nieuw; //verplaats t weer naar de next in struct.

nieuw = new getallenlijst;
nieuw->getal1 = rand();
nieuw->getal2 = rand();
nieuw->getal3 = rand();

temp->next = nieuw; // verplaats t naar voren van oude struct.

temp = nieuw; //verplaats t weer naar de next in struct.
nieuw = new getallenlijst;
nieuw->getal1 = rand();
nieuw->getal2 = rand();
nieuw->getal3 = rand();

temp->next = nieuw;            //verplaats t weer.
nieuw->next = NULL;         //Eindig lijst.


//Loop om getallen uit te printen.
getallenlijst *current = head;
while( current != NULL ) {
cout << (*current).getal1 << endl;
cout << (*current).getal2 << endl;
cout << (*current).getal3 << endl;
current = current->next;
}

//Loop om alle nodes te verwijderen om memory leak te voorkomen.
while( current != NULL ) {
delete nieuw;
current = current->next;
}

                return 0;
}
