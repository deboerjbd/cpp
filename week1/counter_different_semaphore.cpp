#include <iostream>
#include <semaphore.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

using namespace std;


int main()
{

	off_t offset, pa_offset;	

	/* Zet semaphore aan in shared memory zodat alles erbij kan*/
       sem_t *sema; 
		mmap(NULL, sizeof(sema),PROT_READ |PROT_WRITE,MAP_SHARED|MAP_ANONYMOUS, -1,pa_offset);		
		if (sema == MAP_FAILED) {
		perror("mmap");
		exit(EXIT_FAILURE);
} 


		  /* maak/initialiseer semaphore */
		if ( sem_init(sema, 1, 0) < 0) 
	{
		perror("sem_init");
		exit(EXIT_FAILURE);
	}


        const int MAX = 11;		//zet limiet voor for loop.
        int child1,child2;		//initialize wat later childs worden.
        child1=fork();			//fork child1.
        if (child1==0)			//if met for loop voor child1 code.
        {
        printf("Dit is child1 met pid %d \n",getpid());
        for ( int counter = 1; counter < MAX; counter++ )
                {
                cout << counter << " (1)\n";
		if (sem_post(sema) < 0) 
			{
			perror("sem_post");
			}
		sleep(1);
                }
		if (munmap(sema, sizeof(sema)) < 0) {
		perror("munmap");
		exit(EXIT_FAILURE);
		}
		exit(EXIT_SUCCESS);
        }
        else
                {
                child2=fork(); 		//fork child2.
                if(child2==0)		//if met forloop voor child2 code.
                        {
                                printf("Dit is child2 met pid %d \n",getpid());
                                for ( int counter = 1; counter < MAX; counter++ )
                                cout << counter << " (2)\n";
				if (sem_post (sema) < 0)
				{
				perror("sem_post");
				}
				sleep(1);
                        }
			if (munmap(sema, sizeof(sema)) < 0) {
			perror(EXIT_FAILURE);
			exit(EXIT_FAILURE)l
			}
			exit(EXIT_SUCCESS);
                }
        printf("De parent heeft pid %d \n",getpid());	//return parentID.
	return 0;
}
