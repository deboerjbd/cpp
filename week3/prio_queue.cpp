#include <iostream>
#include <string>
#include <queue>
#include <utility>

using namespace std;

 
int main() {
/*
  priority_queue<pair<int, string> > pq;
  pq.push(make_pair(3, "child node"));
  pq.push(make_pair(4, "leaf node 1"));
  pq.push(make_pair(5, "leaf node 2"));
  pq.push(make_pair(1, "root node"));
  pq.push(make_pair(2, "parent node"));
 
  while (!pq.empty()) {
    cout << pq.first().first << ", " << pq.top().first << endl;
    pq.pop();
  }
*/

/*
  vector<pair<int, string> > pq;
  pq.push_back(make_pair(3, "child node"));
  pq.push_back(make_pair(4, "leaf node 1"));
  pq.push_back(make_pair(5, "lead node 2"));
  pq.push_back(make_pair(1, "root"));
 
  // heapify
  make_heap(pq.n(), pq.end());
 
  // enqueue
  pq.push_back(make_pair(2, "parent node"));
  push_heap(pq.begin(), pq.end());
 
  while (!pq.empty()) {
    // peek
    cout << pq[0].first << ", " << pq[0].second << endl;
    // dequeue
    pop_heap(pq.begin(), pq.end());
    pq.pop_back();
 
	}
*/

priority_queue<int> pqtest;

pqtest.push(5);
pqtest.push(2);
pqtest.push(3);
pqtest.push(1);
pqtest.push(4);

cout << "elementen uit poppen (sp?): ";
	while (!pqtest.empty()) {
	cout << ' '  << pqtest.top();
	pqtest.pop();
}

cout << '\n';
return 0;
}
